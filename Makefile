LIB_DIR   := lib
CLASS_DIR := class

MAIN_PACKAGE_PATH    := src/main
MAIN_PACKAGE_NAME    := src.main
CODEGEN_PACKAGE_PATH := src/codeGeneration
CODEGEN_PACKAGE_NAME := src.codeGeneration
PARSER_PACKAGE_PATH  := src/parser
PARSER_PACKAGE_NAME  := src.parser

GRAMMARS := \
	Python3Lexer.g4 \
	Python3Parser.g4

GRAMMARS  := $(addprefix $(PARSER_PACKAGE_PATH)/, $(GRAMMARS))
CLASSPATH := .:$(LIB_DIR)/antlr-4.13.1-complete.jar:$(CLASS_DIR)

all: svm-compile 
	javac -cp $(CLASSPATH) -Xlint:deprecation -d $(CLASS_DIR) $(MAIN_PACKAGE_PATH)/SemanticAnalysisMain.java
	javac -cp $(CLASSPATH) -Xlint:deprecation -d $(CLASS_DIR) $(MAIN_PACKAGE_PATH)/OptimizationMain.java
	javac -cp $(CLASSPATH) -Xlint:deprecation -d $(CLASS_DIR) $(MAIN_PACKAGE_PATH)/Main.java
	javac -cp $(CLASSPATH) -Xlint:deprecation -d $(CLASS_DIR) $(MAIN_PACKAGE_PATH)/ParseAll.java
	javac -cp $(CLASSPATH) -Xlint:deprecation -d $(CLASS_DIR) $(MAIN_PACKAGE_PATH)/CodeGenMain.java

antlr: $(GRAMMARS)
	java -cp $(CLASSPATH) org.antlr.v4.Tool -package $(PARSER_PACKAGE_NAME) -visitor -no-listener $^

svm-antlr: $(CODEGEN_PACKAGE_PATH)/svm/SVM.g4
	java -cp $(CLASSPATH) org.antlr.v4.Tool -package $(CODEGEN_PACKAGE_NAME).svm -visitor -no-listener $^

svm-compile: 
	javac -cp $(CLASSPATH) -d $(CLASS_DIR) $(CODEGEN_PACKAGE_PATH)/svm/TestAssembler.java

svm:
	java -cp $(CLASSPATH) $(CODEGEN_PACKAGE_NAME).svm.TestAssembler $(ARGS)

run:
	java -cp $(CLASSPATH) src.main.Main $(ARGS)

runall:
	mkdir -p trees
	java -cp $(CLASSPATH) src.main.ParseAll

runsem:
	java -cp $(CLASSPATH) src.main.SemanticAnalysisMain $(ARGS)

runopt:
	java -cp $(CLASSPATH) src.main.OptimizationMain $(ARGS)

rungen:
	java -cp $(CLASSPATH) src.main.CodeGenMain $(ARGS)

format:
	npm install prettier-plugin-java --save-dev
	npx prettier --write "**/*.java"

grun:
	java -cp :$(CLASSPATH) org.antlr.v4.gui.TestRig src.parser.Python3 root -gui $(ARGS) &

clean:
	rm -rf $(CLASS_DIR) trees node_modules

.PHONY: all run runall runsem grun clean antlr
