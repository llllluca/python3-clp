Nella cartella `progs/` sono presenti 2 sottocartelle, `ok_sem` contenente i file semanticamente corretti, e `strange_sem` contenente programmi che danno errore nell'analisi semantica statica. 

# Build
```
make
```
## Analisi Semantica
```
make runsem ARGS="path/to/file" 
```
## Ottimizzazione
```
make runopt ARGS="path/to/file"
```
## Generazione bytecode
```
make rungen ARGS="path/to/file"
```
## Esecuzione bytecode
```
make svm ARGS="path/to/file"
```

Federico Borci, Luca Orlandello, Stefano Staffolani.


