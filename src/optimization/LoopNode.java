package src.optimization;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;

public abstract class LoopNode extends Node {

  protected Node body_loop = null;
  protected Node end_loop = null;

  public List<Node> adjacents() {
    LinkedList<Node> l = new LinkedList<Node>();
    if (body_loop != null) l.add(body_loop);
    if (end_loop != null) l.add(end_loop);
    return l;
  }

  public Node print() {
    if (this.visited) {
      return this.end_loop;
    } else {
      System.out.println(this);
      /*
            System.out.println(this + " -- "
                    + "IN: " + INtoString() + " "
                    + "OUT: " + OUTtoString());
            */
      //System.out.println(this + " -- " + "pred: " + this.pred);
      this.visited = true;
      return this.body_loop;
    }
  }

  public Node nextStmt() {
    return this.end_loop;
  }

  public void predecessors() {
    this.visited = true;
    if (this.end_loop != null) {
      this.end_loop.pred.add(this);
      if (!this.end_loop.visited) {
        this.end_loop.predecessors();
      }
    }
    this.body_loop.pred.add(this);
    if (!this.body_loop.visited) {
      this.body_loop.predecessors();
    }
  }

  public AbstractMap.SimpleEntry<Node, Integer> enumerate(int lineCounter) {
    if (this.visited) {
      return new AbstractMap.SimpleEntry<>(this.end_loop, lineCounter);
    } else {
      this.line = lineCounter;
      lineCounter++;
      this.visited = true;
      return new AbstractMap.SimpleEntry<>(this.body_loop, lineCounter);
    }
  }

  public Node addNestingLevel(int nestingLevel) {
    if (this.visited) {
      return this.end_loop;
    } else {
      this.nestingLevel += nestingLevel;
      this.visited = true;
      return this.body_loop;
    }
  }
}
