package src.optimization;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import src.parser.Python3Parser;
import src.parser.Python3Parser.ExprContext;
import src.parser.Python3Parser.For_stmtContext;

public class ForNode extends LoopNode {

  protected Python3Parser.For_stmtContext ctx;
  protected Set<String> vars;

  protected ForNode(For_stmtContext ctx, int nestingLevel) {
    this.ctx = ctx;
    this.nestingLevel = nestingLevel;
    this.vars = this.getVars(ctx);
  }

  private Set<String> getVars(For_stmtContext ctx) {
    HashSet<String> vars = new HashSet<String>();
    ExprVisitor visitor = new ExprVisitor();
    List<ExprContext> exprs = ctx.exprlist().expr();
    int size = exprs.size() - 1;
    for (int i = 0; i < size; i++) {
      vars.addAll(visitor.getNames(exprs.get(i)));
    }
    vars.addAll(visitor.getNames(exprs.get(size).expr(0)));
    return vars;
  }

  public boolean hasWriteAccess(String var) {
    return false;
  }

  public boolean hasReadAccess(String var) {
    ExprVisitor exprVisitor = new ExprVisitor();
    for (ExprContext e : ctx.exprlist().expr()) {
      if (exprVisitor.getNames(e).contains(var)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString() {
    String tabs = "";
    for (int i = 0; i < nestingLevel; i++) {
      tabs += "\t";
    }
    String exprlist = "";
    for (ExprContext e : ctx.exprlist().expr()) {
      exprlist += this.getText(e);
    }
    return tabs + "for " + exprlist + ":";
  }
}
