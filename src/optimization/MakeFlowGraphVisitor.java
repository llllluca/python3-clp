package src.optimization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import src.parser.Python3Parser.BlockContext;
import src.parser.Python3Parser.For_stmtContext;
import src.parser.Python3Parser.FuncdefContext;
import src.parser.Python3Parser.If_stmtContext;
import src.parser.Python3Parser.RootContext;
import src.parser.Python3Parser.Simple_stmtContext;
import src.parser.Python3Parser.Simple_stmtsContext;
import src.parser.Python3Parser.While_stmtContext;
import src.parser.Python3ParserBaseVisitor;

public class MakeFlowGraphVisitor extends Python3ParserBaseVisitor<FlowGraph> {

  private int nestingLevel = 1;

  private FlowGraph mergeGraphs(LinkedList<FlowGraph> graphs) {
    FlowGraph graph = graphs.getFirst();
    Iterator<FlowGraph> iter = graphs.listIterator(1);
    while (iter.hasNext()) {
      graph.merge(iter.next());
    }
    return graph;
  }

  @Override
  public FlowGraph visitRoot(RootContext ctx) {
    LinkedList<FlowGraph> childrenGraphs = new LinkedList<FlowGraph>();
    for (int i = 0; i < ctx.getChildCount(); i++) {
      FlowGraph result = visit(ctx.getChild(i));
      if (result != null) {
        childrenGraphs.add(result);
      }
    }

    return mergeGraphs(childrenGraphs);
  }

  @Override
  public FlowGraph visitSimple_stmts(Simple_stmtsContext ctx) {
    return visit(ctx.simple_stmt(0));
  }

  @Override
  public FlowGraph visitSimple_stmt(Simple_stmtContext ctx) {
    FlowGraph graph;
    if (ctx.assignment() != null) {
      graph = new FlowGraph(ctx.assignment(), nestingLevel);
    } else {
      graph = new FlowGraph(ctx, nestingLevel);
    }
    return graph;
  }

  @Override
  public FlowGraph visitWhile_stmt(While_stmtContext ctx) {
    nestingLevel++;
    FlowGraph bodyGraph = visit(ctx.block(0));
    nestingLevel--;
    return new FlowGraph(ctx, nestingLevel, bodyGraph);
  }

  @Override
  public FlowGraph visitFor_stmt(For_stmtContext ctx) {
    nestingLevel++;
    FlowGraph bodyGraph = visit(ctx.block(0));
    nestingLevel--;
    return new FlowGraph(ctx, nestingLevel, bodyGraph);
  }

  @Override
  public FlowGraph visitIf_stmt(If_stmtContext ctx) {
    nestingLevel++;
    ArrayList<FlowGraph> childrenGraphs = new ArrayList<FlowGraph>();
    for (BlockContext block : ctx.block()) {
      childrenGraphs.add(visit(block));
    }
    nestingLevel--;
    int n = childrenGraphs.size();
    FlowGraph ifBranch = childrenGraphs.get(0);
    FlowGraph elseBranch = null;
    if (ctx.ELSE() != null) {
      elseBranch = childrenGraphs.get(n - 1);
      n--;
    }
    List<FlowGraph> elifBranches = childrenGraphs.subList(1, n);

    return new FlowGraph(ctx, nestingLevel, ifBranch, elifBranches, elseBranch);
  }

  @Override
  public FlowGraph visitBlock(BlockContext ctx) {
    LinkedList<FlowGraph> childrenGraphs = new LinkedList<FlowGraph>();
    for (int i = 0; i < ctx.getChildCount(); i++) {
      FlowGraph result = visit(ctx.getChild(i));
      if (result != null) {
        childrenGraphs.add(result);
      }
    }
    return mergeGraphs(childrenGraphs);
  }
}
