package src.optimization;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import src.parser.Python3Parser.AssignmentContext;
import src.parser.Python3Parser.For_stmtContext;
import src.parser.Python3Parser.If_stmtContext;
import src.parser.Python3Parser.Simple_stmtContext;
import src.parser.Python3Parser.While_stmtContext;

public class FlowGraph {

  protected ArrayList<Node> nodes = new ArrayList<Node>();
  protected Node start = null;
  protected ArrayList<Node> ends = new ArrayList<Node>();

  public FlowGraph(Simple_stmtContext ctx, int nestingLevel) {
    SimpleStmtNode node = new SimpleStmtNode(ctx, nestingLevel);
    this.nodes.add(node);
    this.start = node;
    this.ends.add(node);
  }

  public FlowGraph(AssignmentContext ctx, int nestingLevel) {
    String varName = ctx.exprlist(0).expr(0).atom().NAME().getText();
    AssignmentNode node = new AssignmentNode(ctx, nestingLevel, varName);
    this.nodes.add(node);
    this.start = node;
    this.ends.add(node);
  }

  public FlowGraph(While_stmtContext ctx, int nestingLevel, FlowGraph body) {
    WhileNode node = new WhileNode(ctx, nestingLevel);
    this.nodes.add(node);
    this.start = node;
    this.ends.add(node);
    this.nodes.addAll(body.nodes);
    node.body_loop = body.start;
    for (Node n : body.ends) {
      if (n instanceof WhileNode) {
        ((WhileNode) n).end_loop = node;
      } else if (n instanceof ForNode) {
        ((ForNode) n).end_loop = node;
      } else if (n instanceof SimpleStmtNode) {
        ((SimpleStmtNode) n).next = node;
      } else if (n instanceof IfNode) {
        ((IfNode) n).next = node;
      } else {
        // error
      }
    }
  }

  public FlowGraph(For_stmtContext ctx, int nestingLevel, FlowGraph body) {
    ForNode node = new ForNode(ctx, nestingLevel);
    this.nodes.add(node);
    this.start = node;
    this.ends.add(node);
    this.nodes.addAll(body.nodes);
    node.body_loop = body.start;
    for (Node n : body.ends) {
      if (n instanceof WhileNode) {
        ((WhileNode) n).end_loop = node;
      } else if (n instanceof ForNode) {
        ((ForNode) n).end_loop = node;
      } else if (n instanceof SimpleStmtNode) {
        ((SimpleStmtNode) n).next = node;
      } else if (n instanceof IfNode) {
        ((IfNode) n).next = node;
      } else {
        // error
      }
    }
  }

  public FlowGraph(
    If_stmtContext ctx,
    int nestingLevel,
    FlowGraph ifBranch,
    List<FlowGraph> elifBranches,
    FlowGraph elseBranch
  ) {
    IfNode node = new IfNode(ctx, nestingLevel);
    this.nodes.add(node);
    this.start = node;
    node.ifBranch = ifBranch.start;
    this.nodes.addAll(ifBranch.nodes);
    this.ends.addAll(ifBranch.ends);
    for (FlowGraph g : elifBranches) {
      node.elifBranches.add(g.start);
      this.nodes.addAll(g.nodes);
      this.ends.addAll(g.ends);
    }
    if (elseBranch != null) {
      node.elseBranch = elseBranch.start;
      this.nodes.addAll(elseBranch.nodes);
      this.ends.addAll(elseBranch.ends);
    } else {
      this.ends.add(node);
    }
  }

  @Override
  public String toString() {
    String out = String.format(
      "start: %s (%s)\nend: %s \n",
      System.identityHashCode(start),
      start,
      this.ends
    );
    for (Node n : nodes) {
      if (n instanceof WhileNode) {
        WhileNode w = (WhileNode) n;
        out +=
        String.format(
          "%s (%s) -> [body: %s, (%s), end: %s, (%s)]\n",
          System.identityHashCode(w),
          w,
          System.identityHashCode(w.body_loop),
          w.body_loop,
          System.identityHashCode(w.end_loop),
          w.end_loop
        );
      } else if (n instanceof ForNode) {
        ForNode w = (ForNode) n;
        out +=
        String.format(
          "%s (%s) -> [body: %s, (%s), end: %s, (%s)]\n",
          System.identityHashCode(w),
          w,
          System.identityHashCode(w.body_loop),
          w.body_loop,
          System.identityHashCode(w.end_loop),
          w.end_loop
        );
      } else if (n instanceof SimpleStmtNode) {
        SimpleStmtNode w = (SimpleStmtNode) n;
        out +=
        String.format(
          "%s (%s) -> [next: %s (%s)]\n",
          System.identityHashCode(w),
          w,
          System.identityHashCode(w.next),
          w.next
        );
      } else if (n instanceof IfNode) {
        IfNode w = (IfNode) n;
        out += String.format("%s (%s) -> [", System.identityHashCode(w), w);
        out +=
        String.format(
          "%s (%s),",
          System.identityHashCode(w.ifBranch),
          w.ifBranch
        );
        for (Node node : w.elifBranches) {
          out += String.format("%s (%s),", System.identityHashCode(node), node);
        }
        if (w.elseBranch != null) {
          out +=
          String.format(
            " %s (%s),",
            System.identityHashCode(w.elseBranch),
            w.elseBranch
          );
        }
        if (w.next != null) {
          out +=
          String.format(" %s (%s),", System.identityHashCode(w.next), w.next);
        }
        out += "]\n";
      } else {
        // error
      }
    }
    return out;
  }

  public void printPredecessor() {
    for (Node n : nodes) {
      System.out.println(
        String.format("%s (%s) -> %s", n, System.identityHashCode(n), n.pred)
      );
    }
  }

  public void merge(FlowGraph graph) {
    this.nodes.addAll(graph.nodes);
    for (Node n : this.ends) {
      if (n instanceof WhileNode) {
        ((WhileNode) n).end_loop = graph.start;
      } else if (n instanceof ForNode) {
        ((ForNode) n).end_loop = graph.start;
      } else if (n instanceof SimpleStmtNode) {
        ((SimpleStmtNode) n).next = graph.start;
      } else if (n instanceof IfNode) {
        ((IfNode) n).next = graph.start;
      } else {
        // error
      }
    }
    this.ends = graph.ends;
  }

  public void printProgram() {
    for (Node n : nodes) {
      n.visited = false;
    }
    Node curr = this.start;
    while (curr != null) {
      curr = curr.print();
    }
  }

  public void predecessors() {
    for (Node n : nodes) {
      n.visited = false;
    }
    this.start.predecessors();
  }

  public void enumerateNodes() {
    AbstractMap.SimpleEntry<Node, Integer> result;
    int lineCounter = 0;
    for (Node n : nodes) {
      n.visited = false;
    }
    Node n = this.start;
    while (n != null) {
      result = n.enumerate(lineCounter);
      n = result.getKey();
      lineCounter = result.getValue();
    }
  }
}
