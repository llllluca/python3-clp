package src.optimization;

import java.util.HashSet;
import java.util.List;
import src.parser.Python3Parser;

public class AssignmentNode extends SimpleStmtNode {

  protected String lhs_var;
  protected HashSet<String> rhs_vars;
  protected boolean invariant = false;

  public AssignmentNode(
    Python3Parser.AssignmentContext assignment,
    int nestingLevel,
    String lhs_var
  ) {
    super(
      (Python3Parser.Simple_stmtContext) assignment.getParent(),
      nestingLevel
    );
    this.rhs_vars = (new ExprVisitor()).getNames(assignment);
    this.lhs_var = lhs_var;
  }

  public String getVariable() {
    return lhs_var;
  }

  @Override
  public boolean hasWriteAccess(String var) {
    return lhs_var.equals(var);
  }

  @Override
  public boolean hasReadAccess(String var) {
    return rhs_vars.contains(var);
  }

  // All the reaching definition of var are outside the loop
  private boolean outsideLoop(List<AssignmentNode> reachingDefs, int loopLine) {
    for (AssignmentNode rd : reachingDefs) {
      if (rd.line > loopLine) {
        return false;
      }
    }
    return true;
  }

  // There is exactly one reaching definition of var and the definition is loop-invariant
  private boolean oneLoopInvariantDef(List<AssignmentNode> reachingDefs) {
    return reachingDefs.size() == 1 && reachingDefs.get(0).invariant;
  }

  public boolean isInvariant(int loopLine) {
    for (String var : rhs_vars) {
      List<AssignmentNode> varReachingDefs = this.reachingDefinition.get(var);
      // If var is a parameter function or a global variable, varReachingDefs is null
      if (varReachingDefs == null || varReachingDefs.isEmpty()) return false;
      boolean condition1;
      boolean condition2;
      condition1 = outsideLoop(varReachingDefs, loopLine);
      condition2 = oneLoopInvariantDef(varReachingDefs);
      if (!condition1 && !condition2) return false;
    }
    return true;
  }
}
