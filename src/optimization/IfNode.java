package src.optimization;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import src.parser.Python3Parser;
import src.parser.Python3Parser.ExprContext;

public class IfNode extends Node {

  private Python3Parser.If_stmtContext ctx;
  protected Node ifBranch = null;
  protected ArrayList<Node> elifBranches = new ArrayList<Node>();
  protected Node elseBranch = null;
  protected Node next = null;

  protected IfNode(Python3Parser.If_stmtContext ctx, int nestingLevel) {
    this.ctx = ctx;
    this.nestingLevel = nestingLevel;
  }

  public boolean hasWriteAccess(String var) {
    return false;
  }

  public boolean hasReadAccess(String var) {
    ExprVisitor exprVisitor = new ExprVisitor();
    for (ExprContext e : ctx.expr()) {
      if (exprVisitor.getNames(e).contains(var)) {
        return true;
      }
    }
    return false;
  }

  public List<Node> adjacents() {
    LinkedList<Node> l = new LinkedList<Node>();
    l.add(ifBranch);
    l.addAll(elifBranches);
    if (elseBranch != null) {
      l.add(elseBranch);
    }
    if (next != null) {
      l.add(next);
    }
    return l;
  }

  public Node print() {
    String tabs = "";
    for (int i = 0; i < nestingLevel; i++) {
      tabs += "\t";
    }
    Node curr;

    System.out.println(tabs + "if " + this.getText(ctx.expr(0)) + ":");
    curr = printIfBody(this.ifBranch);

    List<Python3Parser.ExprContext> exprs = ctx
      .expr()
      .subList(1, ctx.expr().size());
    for (int i = 0; i < exprs.size(); i++) {
      System.out.println(tabs + "elif " + this.getText(exprs.get(i)) + ":");
      curr = printIfBody(this.elifBranches.get(i));
    }

    if (elseBranch != null) {
      System.out.println(tabs + "else:");
      curr = printIfBody(this.elseBranch);
    }

    return curr;
  }

  private Node printIfBody(Node branch) {
    Node curr = branch;
    while (curr != null && curr.nestingLevel > this.nestingLevel) {
      curr = curr.print();
    }
    return curr;
  }

  public Node addNestingLevel(int nestingLevel) {
    Node curr;
    curr = addNestingLevelIfBody(this.ifBranch, nestingLevel);

    for (Node n : this.elifBranches) {
      curr = addNestingLevelIfBody(n, nestingLevel);
    }

    if (this.elseBranch != null) {
      curr = addNestingLevelIfBody(this.elseBranch, nestingLevel);
    }
    this.nestingLevel += nestingLevel;
    return curr;
  }

  private Node addNestingLevelIfBody(Node branch, int nestingLevel) {
    Node curr = branch;
    while (curr != null && curr.nestingLevel > this.nestingLevel) {
      curr = curr.addNestingLevel(nestingLevel);
    }
    return curr;
  }

  public Node nextStmt() {
    Node curr = this.ifBranch;
    if (next != null) return next;
    while (curr != null && curr.nestingLevel > this.nestingLevel) {
      curr = curr.nextStmt();
    }
    return curr;
  }

  public void predecessors() {
    this.visited = true;
    this.ifBranch.pred.add(this);
    if (!this.ifBranch.visited) {
      this.ifBranch.predecessors();
    }
    for (Node n : this.elifBranches) {
      n.pred.add(this);
      if (!n.visited) {
        n.predecessors();
      }
    }
    if (this.elseBranch != null) {
      this.elseBranch.pred.add(this);
      if (!this.elseBranch.visited) {
        this.elseBranch.predecessors();
      }
    }
    if (this.next != null) {
      this.next.pred.add(this);
      if (!this.next.visited) {
        this.next.predecessors();
      }
    }
  }

  public AbstractMap.SimpleEntry<Node, Integer> enumerate(int lineCounter) {
    AbstractMap.SimpleEntry<Node, Integer> curr;
    this.line = lineCounter;
    lineCounter++;
    curr = enumerateIfBody(this.ifBranch, lineCounter);

    for (Node n : this.elifBranches) {
      curr = enumerateIfBody(n, curr.getValue());
    }

    if (elseBranch != null) {
      curr = enumerateIfBody(this.elseBranch, curr.getValue());
    }

    return curr;
  }

  private AbstractMap.SimpleEntry<Node, Integer> enumerateIfBody(
    Node branch,
    int lineCounter
  ) {
    AbstractMap.SimpleEntry<Node, Integer> curr = new AbstractMap.SimpleEntry<
      Node,
      Integer
    >(branch, lineCounter);
    Node node = curr.getKey();
    while (node != null && node.nestingLevel > this.nestingLevel) {
      curr = node.enumerate(curr.getValue());
      node = curr.getKey();
    }
    return curr;
  }

  @Override
  public String toString() {
    String ifString = "";
    ifString += ("if " + this.getText(ctx.expr(0)) + ":");
    for (Python3Parser.ExprContext e : ctx
      .expr()
      .subList(1, ctx.expr().size())) {
      ifString += ("elif " + this.getText(e) + ":");
    }
    if (this.elseBranch != null) {
      ifString += "else:";
    }
    return ifString;
  }
}
