package src.optimization;

import java.util.HashSet;
import org.antlr.v4.runtime.ParserRuleContext;
import src.parser.Python3Parser.AssignmentContext;
import src.parser.Python3Parser.AtomContext;
import src.parser.Python3ParserBaseVisitor;

public class ExprVisitor extends Python3ParserBaseVisitor<Void> {

  private HashSet<String> names = new HashSet<String>();

  public HashSet<String> getNames(ParserRuleContext ctx) {
    names = new HashSet<String>();
    visit(ctx);
    return names;
  }

  @Override
  public Void visitAssignment(AssignmentContext ctx) {
    int rhs_exprlist = 1;
    return visit(ctx.exprlist(rhs_exprlist).expr(0));
  }

  @Override
  public Void visitAtom(AtomContext ctx) {
    if (ctx.NAME() != null) {
      String name = ctx.NAME().getSymbol().getText();
      names.add(name);
      return null;
    } else return visitChildren(ctx);
  }
}
