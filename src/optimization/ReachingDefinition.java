package src.optimization;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class ReachingDefinition {

  FlowGraph flowGraph;
  protected ArrayList<LoopNode> loop_nodes = new ArrayList<LoopNode>();

  public ReachingDefinition(FlowGraph flowGraph) {
    this.flowGraph = flowGraph;
  }

  public void calculateGENKILL() {
    Set<AssignmentNode> allAssignments = new HashSet<>();
    for (Node n : flowGraph.nodes) {
      if (n instanceof AssignmentNode) {
        allAssignments.add((AssignmentNode) n);
      }
    }

    for (Node n : flowGraph.nodes) {
      n.GEN.clear();
      n.KILL.clear();

      if (n instanceof AssignmentNode assignment) {
        assignment.GEN.add(assignment);

        for (AssignmentNode assign : allAssignments) {
          if (
            assign.getVariable().equals(assignment.getVariable()) &&
            !assign.equals(assignment)
          ) {
            assignment.KILL.add(assign);
          }
        }
      } else if (n instanceof ForNode forNode) {
        for (AssignmentNode assign : allAssignments) {
          if (forNode.vars.contains(assign.getVariable())) {
            forNode.KILL.add(assign);
          }
        }
      }
    }
  }

  public void reachingDefinition() {
    boolean changed;
    flowGraph.start.OUT.clear();
    for (Node n : flowGraph.nodes) {
      n.OUT.clear();
    }
    do {
      changed = false;
      for (Node n : flowGraph.nodes) {
        // Calcolo dell'insieme IN
        Set<AssignmentNode> newIN = new HashSet<>();
        for (Node p : n.pred) {
          newIN.addAll(p.OUT);
        }

        // Calcolo dell'insieme OUT
        Set<AssignmentNode> newOUT = new HashSet<>(n.GEN);
        Set<AssignmentNode> inMinusKill = new HashSet<>(newIN);
        inMinusKill.removeAll(n.KILL);
        newOUT.addAll(inMinusKill);

        if (!n.OUT.equals(newOUT)) {
          n.OUT = newOUT;
          changed = true;
        }

        n.IN = newIN;
      }
    } while (changed);

    for (Node n : flowGraph.nodes) {
      for (AssignmentNode assign : n.IN) {
        if (n.reachingDefinition.get(assign.lhs_var) == null) {
          n.reachingDefinition.put(
            assign.lhs_var,
            new ArrayList<AssignmentNode>()
          );
        }
        n.reachingDefinition.get(assign.lhs_var).add(assign);
      }
    }
  }

  protected void findLoops() {
    for (Node n : flowGraph.nodes) {
      if (n instanceof LoopNode) {
        this.loop_nodes.add((LoopNode) n);
      }
    }
  }

  public void checkInvariants() {
    flowGraph.predecessors();
    flowGraph.enumerateNodes();
    this.findLoops();
    this.calculateGENKILL();
    this.reachingDefinition();
    for (LoopNode n : loop_nodes) {
      Node iter = n.body_loop;
      boolean changed;
      do {
        changed = false;
        while (iter != n.end_loop) {
          if (iter instanceof AssignmentNode assign) {
            boolean x = n.end_loop != null
              ? readBeforeWrite(n.end_loop, assign.lhs_var)
              : false;
            if (!assign.invariant && assign.isInvariant(n.line) && !x) {
              changed = true;
              assign.invariant = true;
            }
          }

          iter = iter.nextStmt();
        }
      } while (changed);
    }
  }

  private boolean readBeforeWrite(Node node, String var) {
    LinkedList<Node> queue = new LinkedList<Node>();
    for (Node n : flowGraph.nodes) {
      n.visited = false;
    }
    node.visited = true;
    queue.addLast(node);
    while (!queue.isEmpty()) {
      Node v = queue.pollFirst();
      if (v.hasReadAccess(var)) {
        return true;
      }
      if (v.hasWriteAccess(var)) {
        continue;
      }
      for (Node w : v.adjacents()) {
        if (!w.visited) {
          w.visited = true;
          queue.addLast(w);
        }
      }
    }
    return false;
  }

  public void optimizeLoop() {
    this.checkInvariants();
    for (LoopNode n : loop_nodes) {
      moveOutsideLoop(n);
    }
  }

  private LinkedList<Node> getLoopPreds(LoopNode loopNode) {
    LinkedList<Node> newPreds = new LinkedList<Node>();
    for (Node n : loopNode.pred) {
      if (n.line < loopNode.line) {
        newPreds.add(n);
      }
    }
    return newPreds;
  }

  public void moveOutsideLoop(LoopNode loopNode) {
    Node iter = loopNode.body_loop;
    ArrayList<AssignmentNode> invs = new ArrayList<AssignmentNode>();
    while (iter != loopNode.end_loop) {
      if (iter instanceof AssignmentNode assign) {
        if (assign.invariant) {
          invs.add(assign);
          assign.nestingLevel = loopNode.nestingLevel;
          for (Node n : assign.pred) {
            if (n instanceof LoopNode) {
              if (n == loopNode) {
                loopNode.body_loop = assign.next;
              } else {
                ((LoopNode) n).end_loop = assign.next;
              }
            } else if (n instanceof SimpleStmtNode) {
              ((SimpleStmtNode) n).next = assign.next;
            } else if (n instanceof IfNode) {
              ((IfNode) n).next = assign.next;
            } else {
              // errore
            }
          }
          assign.next.pred.remove(assign);
          assign.next.pred.addAll(assign.pred);
        }
      }
      iter = iter.nextStmt();
    }

    if (invs.isEmpty()) return;
    int n = invs.size() - 1;
    for (int i = 0; i < n; i++) {
      invs.get(i).next = invs.get(i + 1);
    }
    invs.get(n).next = loopNode;
    for (Node p : getLoopPreds(loopNode)) {
      if (p instanceof LoopNode) {
        if (p.nestingLevel < loopNode.nestingLevel) ((LoopNode) p).body_loop =
          invs.get(0);
        else ((LoopNode) p).end_loop = invs.get(0);
      } else if (p instanceof SimpleStmtNode) {
        ((SimpleStmtNode) p).next = invs.get(0);
      } else if (p instanceof IfNode) {
        ((IfNode) p).next = invs.get(0);
      } else {
        // errore
      }
    }
  }

  public void addNestingLevel(Node start, Node end, int nestingLevel) {
    for (Node n : flowGraph.nodes) {
      n.visited = false;
    }
    Node curr = start;
    while (curr != end) {
      curr = curr.addNestingLevel(nestingLevel);
    }
  }
}
