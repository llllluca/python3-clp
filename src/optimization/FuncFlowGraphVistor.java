package src.optimization;

import java.util.List;
import src.parser.Python3Parser.BlockContext;
import src.parser.Python3Parser.For_stmtContext;
import src.parser.Python3Parser.FuncdefContext;
import src.parser.Python3Parser.If_stmtContext;
import src.parser.Python3Parser.Simple_stmtContext;
import src.parser.Python3Parser.While_stmtContext;
import src.parser.Python3ParserBaseVisitor;

public class FuncFlowGraphVistor extends Python3ParserBaseVisitor<Void> {

  int nestingLevel = 0;
  MakeFlowGraphVisitor makeGraph = new MakeFlowGraphVisitor();

  @Override
  public Void visitSimple_stmt(Simple_stmtContext ctx) {
    String tabs = "";
    for (int i = 0; i < this.nestingLevel; i++) {
      tabs += '\t';
    }
    System.out.println(tabs + ctx.getText());
    return null;
  }

  @Override
  public Void visitWhile_stmt(While_stmtContext ctx) {
    String tabs = "";
    for (int i = 0; i < this.nestingLevel; i++) {
      tabs += '\t';
    }
    this.nestingLevel++;
    System.out.println(
      tabs + ctx.WHILE().getText() + " " + ctx.expr().getText() + ":"
    );
    visit(ctx.block(0));
    this.nestingLevel--;
    return null;
  }

  @Override
  public Void visitFor_stmt(For_stmtContext ctx) {
    String tabs = "";
    for (int i = 0; i < this.nestingLevel; i++) {
      tabs += '\t';
    }
    this.nestingLevel++;
    System.out.println(
      tabs + ctx.FOR().getText() + " " + ctx.exprlist().getText() + ":"
    );
    visit(ctx.block(0));
    this.nestingLevel--;
    return null;
  }

  @Override
  public Void visitIf_stmt(If_stmtContext ctx) {
    String tabs = "";
    for (int i = 0; i < this.nestingLevel; i++) {
      System.out.println(
        tabs + ctx.IF().getText() + " " + ctx.expr(0).getText() + ":"
      );
      tabs += '\t';
    }
    this.nestingLevel++;
    List<BlockContext> blocks = ctx.block();
    System.out.println(
      tabs + ctx.IF().getText() + " " + ctx.expr(0).getText() + ":"
    );
    visit(blocks.get(0));
    int n = blocks.size() - 1;
    for (int i = 1; i < n; i++) {
      System.out.println(
        tabs +
        ctx.ELIF().get(i - 1).getText() +
        " " +
        ctx.expr(i).getText() +
        ":"
      );
      visit(blocks.get(i));
    }
    if (ctx.ELSE() != null) {
      System.out.println(tabs + ctx.ELSE().getText() + ":");
      visit(blocks.get(n));
    }
    this.nestingLevel--;
    return null;
  }

  @Override
  public Void visitFuncdef(FuncdefContext ctx) {
    FlowGraph blockGraph = makeGraph.visit(ctx.block());
    ReachingDefinition rd = new ReachingDefinition(blockGraph);
    rd.optimizeLoop();
    String paramlist = ctx.paramlist() != null ? ctx.paramlist().getText() : "";
    System.out.print("\n");
    System.out.println(
      ctx.DEF().getText() + " " + ctx.NAME().getText() + "(" + paramlist + "):"
    );
    blockGraph.printProgram();
    System.out.print("\n");
    return null;
  }
}
