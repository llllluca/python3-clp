package src.optimization;

import java.util.*;
import org.antlr.v4.runtime.tree.ParseTree;

public abstract class Node {

  protected Set<AssignmentNode> IN = new HashSet<AssignmentNode>();
  protected Set<AssignmentNode> OUT = new HashSet<AssignmentNode>();

  protected Set<AssignmentNode> GEN = new HashSet<AssignmentNode>();
  protected Set<AssignmentNode> KILL = new HashSet<AssignmentNode>();

  protected HashMap<String, ArrayList<AssignmentNode>> reachingDefinition =
    new HashMap<>();

  protected LinkedList<Node> pred = new LinkedList<Node>();
  protected int nestingLevel = 0;
  protected boolean visited = false;
  protected int line;

  public String getText(ParseTree ctx) {
    if (ctx.getChildCount() == 0) {
      return "";
    } else {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < ctx.getChildCount(); ++i) {
        String space = i == 0 ? "" : " ";
        builder.append(space + ctx.getChild(i).getText());
      }
      return builder.toString();
    }
  }

  @Override
  public abstract String toString();

  public abstract Node print();

  public abstract void predecessors();

  public abstract Node nextStmt();

  public abstract List<Node> adjacents();

  public abstract Node addNestingLevel(int nestingLevel);

  public abstract boolean hasWriteAccess(String var);

  public abstract boolean hasReadAccess(String var);

  public abstract AbstractMap.SimpleEntry<Node, Integer> enumerate(
    int lineCounter
  );

  public String INtoString() {
    LinkedList<Integer> l = new LinkedList<Integer>();
    for (AssignmentNode n : IN) {
      l.add(n.line);
    }
    return l.toString();
  }

  public String OUTtoString() {
    LinkedList<Integer> l = new LinkedList<Integer>();
    for (AssignmentNode n : OUT) {
      l.add(n.line);
    }
    return l.toString();
  }
}
