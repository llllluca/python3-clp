package src.optimization;

import src.parser.Python3Parser;

public class WhileNode extends LoopNode {

  protected Python3Parser.While_stmtContext ctx;

  protected WhileNode(Python3Parser.While_stmtContext ctx, int nestingLevel) {
    this.ctx = ctx;
    this.nestingLevel = nestingLevel;
  }

  public boolean hasWriteAccess(String var) {
    return false;
  }

  public boolean hasReadAccess(String var) {
    ExprVisitor exprVisitor = new ExprVisitor();
    return exprVisitor.getNames(ctx.expr()).contains(var);
  }

  @Override
  public String toString() {
    String tabs = "";
    for (int i = 0; i < nestingLevel; i++) {
      tabs += "\t";
    }
    return (tabs + "while " + ctx.expr().getText() + ":");
  }
}
