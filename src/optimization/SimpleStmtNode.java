package src.optimization;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;
import org.antlr.v4.runtime.tree.ParseTree;
import src.parser.Python3Parser;
import src.parser.Python3Parser.Import_stmContext;
import src.parser.Python3Parser.Return_stmtContext;

public class SimpleStmtNode extends Node {

  private Python3Parser.Simple_stmtContext simple_stmt;
  public Node next = null;

  public SimpleStmtNode(
    Python3Parser.Simple_stmtContext simple_stmt,
    int nestingLevel
  ) {
    this.simple_stmt = simple_stmt;
    this.nestingLevel = nestingLevel;
  }

  public boolean hasWriteAccess(String var) {
    return false;
  }

  public boolean hasReadAccess(String var) {
    ExprVisitor exprVisitor = new ExprVisitor();
    return exprVisitor.getNames(simple_stmt).contains(var);
  }

  public List<Node> adjacents() {
    LinkedList<Node> l = new LinkedList<Node>();
    if (next != null) l.add(next);
    return l;
  }

  public Node print() {
    /*
            System.out.println(this.toString() + " -- "
                    + "pred: " + pred);
            */
    System.out.println(this);

    /*
        System.out.println(this.toString() + " -- "
                + "IN: " + INtoString() + " "
                + "OUT: " + OUTtoString());
        */

    /*
            System.out.println(this.toString() + " -- "
                    + "gen: "  + this.GEN.toString() + ", "
                    + "kill: " + this.KILL.toString());
            */
    return this.next;
  }

  public Node nextStmt() {
    return this.next;
  }

  public void predecessors() {
    this.visited = true;
    if (this.next != null) {
      this.next.pred.add(this);
      if (!this.next.visited) {
        this.next.predecessors();
      }
    }
  }

  public AbstractMap.SimpleEntry<Node, Integer> enumerate(int lineCounter) {
    this.line = lineCounter;
    lineCounter++;
    return new AbstractMap.SimpleEntry<>(this.next, lineCounter);
  }

  public Node addNestingLevel(int nestingLevel) {
    this.nestingLevel += nestingLevel;
    return this.next;
  }

  public String toString() {
    String tabs = "";
    for (int i = 0; i < nestingLevel; i++) {
      tabs += "\t";
    }
    String simple_stmt_text;
    ParseTree firstChild = simple_stmt.getChild(0);
    if (
      (firstChild instanceof Return_stmtContext) ||
      (firstChild instanceof Import_stmContext)
    ) {
      simple_stmt_text = this.getText(firstChild);
    } else {
      simple_stmt_text = simple_stmt.getText();
    }
    return tabs + simple_stmt_text;
  }

  public Python3Parser.Simple_stmtContext getSimple_stmt() {
    return simple_stmt;
  }
}
