package src.semanticAnalysis;

public class VariableSymbol extends Symbol {

  int offset;

  public VariableSymbol(String name, Scope scope, int offset) {
    super(name, scope);
    this.offset = offset;
  }

  @Override
  public String toString() {
    return "Var:" + super.toString() + "," + offset;
  }

  public int getOffset() {
    return offset;
  }
}
