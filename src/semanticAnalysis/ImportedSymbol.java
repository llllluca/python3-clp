package src.semanticAnalysis;

public class ImportedSymbol extends Symbol {

  public ImportedSymbol(String name, Scope scope) {
    super(name, scope);
  }

  @Override
  public String toString() {
    return "Imported:" + super.toString();
  }
}
