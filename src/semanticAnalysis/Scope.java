package src.semanticAnalysis;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class Scope {

  Scope enclosingScope; // null if global (outermost) scope
  Map<String, Symbol> symbols = new LinkedHashMap<String, Symbol>();

  public Scope(Scope enclosingScope) {
    this.enclosingScope = enclosingScope;
  }

  public Symbol rec_resolve_until_function_scope(String name) {
    Symbol s = symbols.get(name);
    if (s != null) {
      return s;
    }
    // if not here, check any enclosing scope
    if (!(this instanceof FunctionScope) && enclosingScope != null) {
      return enclosingScope.rec_resolve_until_function_scope(name);
    }
    return null; // not found
  }

  public Symbol resolve_recursively(String name) {
    Symbol s = symbols.get(name);
    if (s != null) {
      return s;
    }
    // if not here, check any enclosing scope
    if (enclosingScope != null) {
      return enclosingScope.resolve_recursively(name);
    }
    return null; // not found
  }

  public Symbol resolve(String name) {
    return symbols.get(name);
  }

  public void define(Symbol sym) {
    symbols.put(sym.getName(), sym);
  }

  public Scope getEnclosingScope() {
    return enclosingScope;
  }

  public int getNestingLevel() {
    if (enclosingScope == null) {
      return 0;
    } else if (this instanceof FunctionScope) {
      return 1 + enclosingScope.getNestingLevel();
    } else return enclosingScope.getNestingLevel();
  }

  @Override
  public String toString() {
    return symbols.toString();
  }
}
