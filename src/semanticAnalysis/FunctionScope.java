package src.semanticAnalysis;

public class FunctionScope extends Scope {

  public FunctionScope(Scope enclosingScope) {
    super(enclosingScope);
  }

  @Override
  public String toString() {
    return "F:" + symbols.toString();
  }
}
