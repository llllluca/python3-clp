package src.semanticAnalysis;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;
import src.parser.Python3Parser;
import src.parser.Python3ParserBaseVisitor;

public class SemanticAnalysisVisitor extends Python3ParserBaseVisitor<Void> {

  public ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
  public ParseTreeProperty<Integer> offset = new ParseTreeProperty<Integer>();
  public Scope currentScope;
  int currentOffset = 1;

  public SemanticAnalysisVisitor(Scope globalScope) {
    currentScope = globalScope;
  }

  @Override
  public Void visitRoot(Python3Parser.RootContext ctx) {
    visitChildren(ctx);
    this.offset.put(ctx, currentOffset - 1);
    return null;
  }

  @Override
  public Void visitAssignment(Python3Parser.AssignmentContext ctx) {
    int lhs_exprlist = 0;
    int rhs_exprlist = 1;
    visit(ctx.exprlist(rhs_exprlist));
    for (Python3Parser.ExprContext e : ctx.exprlist(lhs_exprlist).expr()) {
      Python3Parser.AtomContext atom = e.atom();
      if (atom == null || atom.NAME() == null) {
        System.err.println(
          String.format(
            "Error: `%s', left hand side of an assignment must be variables!",
            e.getText()
          )
        );
        continue;
      }
      String name = atom.NAME().getSymbol().getText();
      TerminalNode assign = ctx.augassign().ASSIGN();
      Symbol s = currentScope.rec_resolve_until_function_scope(name);
      if (assign != null && s == null) {
        currentScope.define(
          new VariableSymbol(name, currentScope, currentOffset)
        );
        currentOffset++;
      } else if (assign == null) {
        if (s == null) {
          System.err.println(
            String.format("Error: `%s', undefined variable!", name)
          );
        }
      }
    }
    return null;
  }

  @Override
  public Void visitExpr(Python3Parser.ExprContext ctx) {
    // l'espressione è una chiamata di funzione del tipo NAME(par1, ..., parN)
    if (
      ctx.atom() != null &&
      ctx.trailer().size() > 1 &&
      ctx.trailer(0).OPEN_PAREN() != null &&
      ctx.trailer(0).CLOSE_PAREN() != null
    ) {
      String name = ctx.atom().NAME().getSymbol().getText();
      Symbol s = currentScope.resolve_recursively(name);
      if (s == null) {
        System.err.println(
          String.format("Error: `%s', undefined symbol!", name)
        );
      } else if (!(s instanceof FuncSymbol || s instanceof ImportedSymbol)) {
        System.err.println(
          String.format("Error: `%s', is not a function!", name)
        );
      }
    } else visitChildren(ctx);
    return null;
  }

  @Override
  public Void visitAtom(Python3Parser.AtomContext ctx) {
    if (ctx.NAME() != null) {
      String name = ctx.NAME().getSymbol().getText();
      Symbol s = currentScope.resolve_recursively(name);
      if (s == null) {
        System.err.println(
          String.format("Error: `%s', undefined symbol!", name)
        );
      }
    } else visitChildren(ctx);
    return null;
  }

  @Override
  public Void visitImport_stm(Python3Parser.Import_stmContext ctx) {
    if (ctx.FROM() == null) {
      // Gestisci 'import' dotted_name ('as' NAME)?'
      visitImportDottedNameAs(ctx);
    } else if (ctx.STAR() == null) {
      // Gestisci 'from' dotted_name 'import' NAME  (',' NAME)*'
      visitFromDottedNameImportNames(ctx);
    }
    return null;
  }

  private void visitFromDottedNameImportNames(
    Python3Parser.Import_stmContext ctx
  ) {
    // Gestisci 'from' dotted_name 'import' NAME  (',' NAME)*'
    for (TerminalNode name : ctx.NAME()) {
      String importName = name.getText();
      Symbol s = currentScope.resolve_recursively(importName);
      if (s == null) {
        currentScope.define(new ImportedSymbol(importName, currentScope));
      } else {
        System.err.println(
          String.format("Error: `%s' is an already defined symbol!", importName)
        );
      }
    }
  }

  private void visitImportDottedNameAs(Python3Parser.Import_stmContext ctx) {
    // Gestisci 'import' dotted_name ('as' NAME)?'
    String moduleName = ctx.dotted_name().NAME(0).getSymbol().getText();
    // Se ho AS prendo il NAME dopo AS altrimenti mantengo moduleName
    String alias = (ctx.AS() != null) ? ctx.NAME(0).getText() : moduleName;
    Symbol s = currentScope.resolve_recursively(alias);
    if (s == null) {
      currentScope.define(new ModuleSymbol(alias, currentScope));
    } else {
      System.err.println(
        String.format("Error: `%s' is an already defined symbol!", alias)
      );
    }
  }

  @Override
  public Void visitBlock(Python3Parser.BlockContext ctx) {
    currentScope = new BlockScope(currentScope);
    scopes.put(ctx, currentScope);
    visitChildren(ctx);
    currentScope = currentScope.getEnclosingScope();
    return null;
  }

  @Override
  public Void visitFuncdef(Python3Parser.FuncdefContext ctx) {
    currentScope = new FunctionScope(currentScope);
    scopes.put(ctx, currentScope);
    int oldOffset = this.currentOffset;
    this.currentOffset = 1;
    visitChildren(ctx);
    this.offset.put(ctx, currentOffset - 1);
    this.currentOffset = oldOffset;
    currentScope = currentScope.getEnclosingScope();
    return null;
  }

  @Override
  public Void visitFor_stmt(Python3Parser.For_stmtContext ctx) {
    currentScope = new BlockScope(currentScope);
    scopes.put(ctx, currentScope);

    visitExprlistFor(ctx.exprlist());
    visitChildren(ctx.block(0));
    if (ctx.ELSE() != null) {
      visitChildren(ctx.block(1));
    }

    currentScope = currentScope.getEnclosingScope();
    return null;
  }

  // Metodo per visitare exprlist nel contesto del ciclo for
  private void visitExprlistFor(Python3Parser.ExprlistContext ctx) {
    int i;
    for (i = 0; i < (ctx.expr().size() - 1); i++) {
      Python3Parser.ExprContext e = ctx.expr(i);
      Python3Parser.AtomContext atom = e.atom();
      if (atom == null || atom.NAME() == null) {
        System.err.println(
          String.format(
            "Error: `%s', left hand side of an assignment must be variables!",
            e.getText()
          )
        );
        continue;
      }
      String name = atom.NAME().getSymbol().getText();
      if (currentScope.rec_resolve_until_function_scope(name) == null) {
        currentScope.define(
          new VariableSymbol(name, currentScope, this.currentOffset)
        );
        this.currentOffset++;
      }
    }

    Python3Parser.ExprContext exp = ctx.expr(i); // i è nell'ultima posizione della lista
    Python3Parser.Comp_opContext exprcomp_op = exp.comp_op();
    if (exprcomp_op != null && exprcomp_op.IN() != null) {
      Python3Parser.AtomContext atom = exp.expr(0).atom();
      if (atom == null || atom.NAME() == null) {
        System.err.println(
          String.format(
            "Error: `%s', left hand side of an assignment must be variables!",
            exp.getText()
          )
        );
        return;
      } else {
        String name = atom.NAME().getSymbol().getText();
        if (currentScope.rec_resolve_until_function_scope(name) == null) {
          currentScope.define(
            new VariableSymbol(name, currentScope, this.currentOffset)
          );
          this.currentOffset++;
        }
      }
      visit(exp.expr(1));
    }
  }

  public Void visitParamlist(Python3Parser.ParamlistContext ctx) {
    for (Python3Parser.ParamdefContext e : ctx.paramdef()) {
      String name = e.NAME().getSymbol().getText();
      if (currentScope.rec_resolve_until_function_scope(name) != null) {
        System.err.println(
          String.format("Error: `%s', already defined in this scope!", name)
        );
      } else {
        currentScope.define(
          new VariableSymbol(name, currentScope, this.currentOffset)
        );
        this.currentOffset++;
      }
    }
    return null;
  }

  public Void visitComp_for(Python3Parser.Comp_forContext ctx) {
    for (Python3Parser.ExprContext e : ctx.exprlist().expr()) {
      Python3Parser.AtomContext atom = e.atom();
      if (atom == null || atom.NAME() == null) {
        System.err.println(
          String.format(
            "Error: `%s', exprlist in comp_for must be variables!",
            e.getText()
          )
        );
        continue;
      }
      String name = atom.NAME().getSymbol().getText();
      Symbol s = currentScope.resolve(name);
      if (s == null) {
        currentScope.define(
          new VariableSymbol(name, currentScope, this.currentOffset)
        );
        this.currentOffset++;
      } else {
        System.err.println(
          String.format("Error: `%s' is an already defined symbol!", name)
        );
      }
    }
    visit(ctx.expr());
    Python3Parser.Comp_iterContext comp_iter = ctx.comp_iter();
    if (comp_iter != null) {
      visit(comp_iter);
    }
    return null;
  }

  @Override
  public Void visitTestlist_comp(Python3Parser.Testlist_compContext ctx) {
    Python3Parser.Comp_forContext comp_for = ctx.comp_for();
    if (comp_for != null) {
      currentScope = new BlockScope(currentScope);
      scopes.put(ctx, currentScope);
      visit(comp_for);
      visit(ctx.expr(0));
      currentScope = currentScope.getEnclosingScope();
    } else visitChildren(ctx);
    return null;
  }

  @Override
  public Void visitArgument(Python3Parser.ArgumentContext ctx) {
    if (ctx.ASSIGN() != null) {
      visit(ctx.expr(1));
    } else if (ctx.comp_for() != null) {
      currentScope = new BlockScope(currentScope);
      scopes.put(ctx, currentScope);
      visit(ctx.comp_for());
      visit(ctx.expr(0));
      currentScope = currentScope.getEnclosingScope();
    } else visitChildren(ctx);
    return null;
  }
}
