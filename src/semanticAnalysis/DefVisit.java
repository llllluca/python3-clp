package src.semanticAnalysis;

import src.parser.Python3Parser;
import src.parser.Python3ParserBaseVisitor;

public class DefVisit extends Python3ParserBaseVisitor<Void> {

  private Scope globalScope = new BlockScope(null);
  private int count = 0;

  private String[] builtInFunctions = {
    "abs",
    "all",
    "any",
    "ascii",
    "bin",
    "bool",
    "breakpoint",
    "bytearray",
    "bytes",
    "callable",
    "chr",
    "classmethod",
    "compile",
    "complex",
    "delattr",
    "dict",
    "dir",
    "divmod",
    "enumerate",
    "eval",
    "exec",
    "filter",
    "float",
    "format",
    "frozenset",
    "getattr",
    "globals",
    "hasattr",
    "hash",
    "help",
    "hex",
    "id",
    "input",
    "int",
    "isinstance",
    "issubclass",
    "iter",
    "len",
    "list",
    "locals",
    "map",
    "max",
    "memoryview",
    "min",
    "next",
    "object",
    "oct",
    "open",
    "ord",
    "pow",
    "print",
    "property",
    "range",
    "repr",
    "reversed",
    "round",
    "set",
    "setattr",
    "slice",
    "sorted",
    "staticmethod",
    "str",
    "sum",
    "super",
    "tuple",
    "type",
    "vars",
    "zip",
    "__import__",
    "continue",
    "break",
  };

  public DefVisit() {
    for (String function : builtInFunctions) {
      globalScope.define(new ImportedSymbol(function, globalScope));
    }
  }

  public Scope getGlobalScope() {
    return globalScope;
  }

  public String newLabel() {
    String base = "func";
    return base + count++;
  }

  @Override
  public Void visitFuncdef(Python3Parser.FuncdefContext ctx) {
    // first count the number of params
    String name = ctx.NAME().getSymbol().getText();
    int paramLength;
    if (ctx.paramlist() == null) paramLength = 0;
    else paramLength = ctx.paramlist().paramdef().size();
    Symbol s = globalScope.resolve(name);
    if (s == null) {
      globalScope.define(
        new FuncSymbol(name, globalScope, paramLength, this.newLabel())
      );
    } else {
      System.err.println(
        String.format("Error: function `%s` already defined!", name)
      );
    }
    return null;
  }
}
