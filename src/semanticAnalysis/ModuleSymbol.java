package src.semanticAnalysis;

public class ModuleSymbol extends Symbol {

  public ModuleSymbol(String name, Scope scope) {
    super(name, scope);
  }

  @Override
  public String toString() {
    return "Module:" + super.toString();
  }
}
