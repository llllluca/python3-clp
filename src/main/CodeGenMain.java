package src.main;

import java.io.IOException;
import java.io.PrintWriter;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import src.codeGeneration.CodeGenVisitor;
import src.parser.Python3Lexer;
import src.parser.Python3Parser;
import src.semanticAnalysis.DefVisit;
import src.semanticAnalysis.Scope;
import src.semanticAnalysis.SemanticAnalysisVisitor;

public class CodeGenMain {

  public static void main(String[] args) throws IOException {
    CharStream cs = CharStreams.fromFileName(args[0]);
    Python3Lexer lexer = new Python3Lexer(cs);
    CommonTokenStream tokenStream = new CommonTokenStream(lexer);
    Python3Parser parser = new Python3Parser(tokenStream);
    Python3Parser.RootContext tree = parser.root();
    // analisi semantica
    DefVisit defvisitor = new DefVisit();
    defvisitor.visit(tree);
    Scope globalScope = defvisitor.getGlobalScope();
    SemanticAnalysisVisitor Semvisitor = new SemanticAnalysisVisitor(
      globalScope
    );
    Semvisitor.visit(tree);
    // generazione codice
    CodeGenVisitor visitor = new CodeGenVisitor(Semvisitor);
    String code = visitor.visit(tree);
    PrintWriter writer = new PrintWriter("a.out");
    writer.println(code);
    writer.close();
    //System.out.println(code);
  }
}
