package src.main;

import java.io.IOException;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import src.parser.Python3Lexer;
import src.parser.Python3Parser;
import src.semanticAnalysis.DefVisit;
import src.semanticAnalysis.Scope;
import src.semanticAnalysis.SemanticAnalysisVisitor;

public class SemanticAnalysisMain {

  public static void main(String[] args) throws IOException {
    CharStream cs = CharStreams.fromFileName(args[0]);
    Python3Lexer lexer = new Python3Lexer(cs);
    CommonTokenStream tokenStream = new CommonTokenStream(lexer);
    Python3Parser parser = new Python3Parser(tokenStream);
    Python3Parser.RootContext tree = parser.root();
    DefVisit defvisitor = new DefVisit();
    defvisitor.visit(tree);
    Scope globalScope = defvisitor.getGlobalScope();
    SemanticAnalysisVisitor visitor = new SemanticAnalysisVisitor(globalScope);
    visitor.visit(tree);
  }
}
