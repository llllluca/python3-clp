package src.main;

import java.io.IOException;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import src.optimization.FuncFlowGraphVistor;
import src.parser.Python3Lexer;
import src.parser.Python3Parser;

public class OptimizationMain {

  public static void main(String[] args) throws IOException {
    CharStream cs = CharStreams.fromFileName(args[0]);
    Python3Lexer lexer = new Python3Lexer(cs);
    CommonTokenStream tokenStream = new CommonTokenStream(lexer);
    Python3Parser parser = new Python3Parser(tokenStream);
    Python3Parser.RootContext tree = parser.root();

    FuncFlowGraphVistor funvis = new FuncFlowGraphVistor();
    funvis.visit(tree);
  }
}
