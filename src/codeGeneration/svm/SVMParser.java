// Generated from D:/GitHub/clp/python3-clp/src/codeGeneration/svm/SVM.g4 by ANTLR 4.13.1
package src.codeGeneration.svm;

import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.*;

@SuppressWarnings(
  { "all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue" }
)
public class SVMParser extends Parser {
  static {
    RuntimeMetaData.checkVersion("4.13.1", RuntimeMetaData.VERSION);
  }

  protected static final DFA[] _decisionToDFA;
  protected static final PredictionContextCache _sharedContextCache =
    new PredictionContextCache();
  public static final int T__0 = 1, T__1 = 2, T__2 = 3, LOAD = 4, STORE =
    5, STOREI = 6, MOVE = 7, ADD = 8, ADDI = 9, SUB = 10, SUBI = 11, MUL =
    12, MULI = 13, DIV = 14, DIVI = 15, DIVINT = 16, DIVMOD = 17, PUSH =
    18, PUSHR = 19, POP = 20, POPR = 21, BRANCH = 22, BRANCHEQ =
    23, BRANCHLESS = 24, BRANCHLESSEQ = 25, JUMPSUB = 26, RETURNSUB = 27, HALT =
    28, REG = 29, LABEL = 30, NUMBER = 31, WHITESP = 32, LINECOMENTS = 33, ERR =
    34;
  public static final int RULE_assembly = 0, RULE_instruction = 1;

  private static String[] makeRuleNames() {
    return new String[] { "assembly", "instruction" };
  }

  public static final String[] ruleNames = makeRuleNames();

  private static String[] makeLiteralNames() {
    return new String[] {
      null,
      "'('",
      "')'",
      "':'",
      "'load'",
      "'store'",
      "'storei'",
      "'move'",
      "'add'",
      "'addi'",
      "'sub'",
      "'subi'",
      "'mul'",
      "'muli'",
      "'div'",
      "'divi'",
      "'divint'",
      "'divmod'",
      "'push'",
      "'pushr'",
      "'pop'",
      "'popr'",
      "'b'",
      "'beq'",
      "'bl'",
      "'bleq'",
      "'jsub'",
      "'rsub'",
      "'halt'",
    };
  }

  private static final String[] _LITERAL_NAMES = makeLiteralNames();

  private static String[] makeSymbolicNames() {
    return new String[] {
      null,
      null,
      null,
      null,
      "LOAD",
      "STORE",
      "STOREI",
      "MOVE",
      "ADD",
      "ADDI",
      "SUB",
      "SUBI",
      "MUL",
      "MULI",
      "DIV",
      "DIVI",
      "DIVINT",
      "DIVMOD",
      "PUSH",
      "PUSHR",
      "POP",
      "POPR",
      "BRANCH",
      "BRANCHEQ",
      "BRANCHLESS",
      "BRANCHLESSEQ",
      "JUMPSUB",
      "RETURNSUB",
      "HALT",
      "REG",
      "LABEL",
      "NUMBER",
      "WHITESP",
      "LINECOMENTS",
      "ERR",
    };
  }

  private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
  public static final Vocabulary VOCABULARY = new VocabularyImpl(
    _LITERAL_NAMES,
    _SYMBOLIC_NAMES
  );

  /**
   * @deprecated Use {@link #VOCABULARY} instead.
   */
  @Deprecated
  public static final String[] tokenNames;

  static {
    tokenNames = new String[_SYMBOLIC_NAMES.length];
    for (int i = 0; i < tokenNames.length; i++) {
      tokenNames[i] = VOCABULARY.getLiteralName(i);
      if (tokenNames[i] == null) {
        tokenNames[i] = VOCABULARY.getSymbolicName(i);
      }

      if (tokenNames[i] == null) {
        tokenNames[i] = "<INVALID>";
      }
    }
  }

  @Override
  @Deprecated
  public String[] getTokenNames() {
    return tokenNames;
  }

  @Override
  public Vocabulary getVocabulary() {
    return VOCABULARY;
  }

  @Override
  public String getGrammarFileName() {
    return "SVM.g4";
  }

  @Override
  public String[] getRuleNames() {
    return ruleNames;
  }

  @Override
  public String getSerializedATN() {
    return _serializedATN;
  }

  @Override
  public ATN getATN() {
    return _ATN;
  }

  public SVMParser(TokenStream input) {
    super(input);
    _interp = new ParserATNSimulator(
      this,
      _ATN,
      _decisionToDFA,
      _sharedContextCache
    );
  }

  @SuppressWarnings("CheckReturnValue")
  public static class AssemblyContext extends ParserRuleContext {

    public List<InstructionContext> instruction() {
      return getRuleContexts(InstructionContext.class);
    }

    public InstructionContext instruction(int i) {
      return getRuleContext(InstructionContext.class, i);
    }

    public AssemblyContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }

    @Override
    public int getRuleIndex() {
      return RULE_assembly;
    }

    @Override
    public void enterRule(ParseTreeListener listener) {
      if (
        listener instanceof SVMListener
      ) ((SVMListener) listener).enterAssembly(this);
    }

    @Override
    public void exitRule(ParseTreeListener listener) {
      if (
        listener instanceof SVMListener
      ) ((SVMListener) listener).exitAssembly(this);
    }

    @Override
    public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
      if (visitor instanceof SVMVisitor) return (
        (SVMVisitor<? extends T>) visitor
      ).visitAssembly(this);
      else return visitor.visitChildren(this);
    }
  }

  public final AssemblyContext assembly() throws RecognitionException {
    AssemblyContext _localctx = new AssemblyContext(_ctx, getState());
    enterRule(_localctx, 0, RULE_assembly);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
        setState(7);
        _errHandler.sync(this);
        _la = _input.LA(1);
        while ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1610612720L) != 0)) {
          {
            {
              setState(4);
              instruction();
            }
          }
          setState(9);
          _errHandler.sync(this);
          _la = _input.LA(1);
        }
      }
    } catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    } finally {
      exitRule();
    }
    return _localctx;
  }

  @SuppressWarnings("CheckReturnValue")
  public static class InstructionContext extends ParserRuleContext {

    public Token n;
    public Token l;

    public TerminalNode LOAD() {
      return getToken(SVMParser.LOAD, 0);
    }

    public List<TerminalNode> REG() {
      return getTokens(SVMParser.REG);
    }

    public TerminalNode REG(int i) {
      return getToken(SVMParser.REG, i);
    }

    public TerminalNode NUMBER() {
      return getToken(SVMParser.NUMBER, 0);
    }

    public TerminalNode STORE() {
      return getToken(SVMParser.STORE, 0);
    }

    public TerminalNode STOREI() {
      return getToken(SVMParser.STOREI, 0);
    }

    public TerminalNode MOVE() {
      return getToken(SVMParser.MOVE, 0);
    }

    public TerminalNode ADD() {
      return getToken(SVMParser.ADD, 0);
    }

    public TerminalNode ADDI() {
      return getToken(SVMParser.ADDI, 0);
    }

    public TerminalNode SUB() {
      return getToken(SVMParser.SUB, 0);
    }

    public TerminalNode SUBI() {
      return getToken(SVMParser.SUBI, 0);
    }

    public TerminalNode MUL() {
      return getToken(SVMParser.MUL, 0);
    }

    public TerminalNode MULI() {
      return getToken(SVMParser.MULI, 0);
    }

    public TerminalNode DIV() {
      return getToken(SVMParser.DIV, 0);
    }

    public TerminalNode DIVINT() {
      return getToken(SVMParser.DIVINT, 0);
    }

    public TerminalNode DIVMOD() {
      return getToken(SVMParser.DIVMOD, 0);
    }

    public TerminalNode DIVI() {
      return getToken(SVMParser.DIVI, 0);
    }

    public TerminalNode PUSH() {
      return getToken(SVMParser.PUSH, 0);
    }

    public TerminalNode PUSHR() {
      return getToken(SVMParser.PUSHR, 0);
    }

    public TerminalNode POP() {
      return getToken(SVMParser.POP, 0);
    }

    public TerminalNode POPR() {
      return getToken(SVMParser.POPR, 0);
    }

    public TerminalNode BRANCH() {
      return getToken(SVMParser.BRANCH, 0);
    }

    public TerminalNode LABEL() {
      return getToken(SVMParser.LABEL, 0);
    }

    public TerminalNode BRANCHEQ() {
      return getToken(SVMParser.BRANCHEQ, 0);
    }

    public TerminalNode BRANCHLESS() {
      return getToken(SVMParser.BRANCHLESS, 0);
    }

    public TerminalNode BRANCHLESSEQ() {
      return getToken(SVMParser.BRANCHLESSEQ, 0);
    }

    public TerminalNode JUMPSUB() {
      return getToken(SVMParser.JUMPSUB, 0);
    }

    public TerminalNode RETURNSUB() {
      return getToken(SVMParser.RETURNSUB, 0);
    }

    public TerminalNode HALT() {
      return getToken(SVMParser.HALT, 0);
    }

    public InstructionContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }

    @Override
    public int getRuleIndex() {
      return RULE_instruction;
    }

    @Override
    public void enterRule(ParseTreeListener listener) {
      if (
        listener instanceof SVMListener
      ) ((SVMListener) listener).enterInstruction(this);
    }

    @Override
    public void exitRule(ParseTreeListener listener) {
      if (
        listener instanceof SVMListener
      ) ((SVMListener) listener).exitInstruction(this);
    }

    @Override
    public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
      if (visitor instanceof SVMVisitor) return (
        (SVMVisitor<? extends T>) visitor
      ).visitInstruction(this);
      else return visitor.visitChildren(this);
    }
  }

  public final InstructionContext instruction() throws RecognitionException {
    InstructionContext _localctx = new InstructionContext(_ctx, getState());
    enterRule(_localctx, 2, RULE_instruction);
    try {
      enterOuterAlt(_localctx, 1);
      {
        setState(89);
        _errHandler.sync(this);
        switch (_input.LA(1)) {
          case LOAD:
            {
              setState(10);
              match(LOAD);
              setState(11);
              match(REG);
              setState(12);
              match(NUMBER);
              setState(13);
              match(T__0);
              setState(14);
              match(REG);
              setState(15);
              match(T__1);
            }
            break;
          case STORE:
            {
              setState(16);
              match(STORE);
              setState(17);
              match(REG);
              setState(18);
              match(NUMBER);
              setState(19);
              match(T__0);
              setState(20);
              match(REG);
              setState(21);
              match(T__1);
            }
            break;
          case STOREI:
            {
              setState(22);
              match(STOREI);
              setState(23);
              match(REG);
              setState(24);
              match(NUMBER);
            }
            break;
          case MOVE:
            {
              setState(25);
              match(MOVE);
              setState(26);
              match(REG);
              setState(27);
              match(REG);
            }
            break;
          case ADD:
            {
              setState(28);
              match(ADD);
              setState(29);
              match(REG);
              setState(30);
              match(REG);
            }
            break;
          case ADDI:
            {
              setState(31);
              match(ADDI);
              setState(32);
              match(REG);
              setState(33);
              match(NUMBER);
            }
            break;
          case SUB:
            {
              setState(34);
              match(SUB);
              setState(35);
              match(REG);
              setState(36);
              match(REG);
            }
            break;
          case SUBI:
            {
              setState(37);
              match(SUBI);
              setState(38);
              match(REG);
              setState(39);
              match(NUMBER);
            }
            break;
          case MUL:
            {
              setState(40);
              match(MUL);
              setState(41);
              match(REG);
              setState(42);
              match(REG);
            }
            break;
          case MULI:
            {
              setState(43);
              match(MULI);
              setState(44);
              match(REG);
              setState(45);
              match(NUMBER);
            }
            break;
          case DIV:
            {
              setState(46);
              match(DIV);
              setState(47);
              match(REG);
              setState(48);
              match(REG);
            }
            break;
          case DIVINT:
            {
              setState(49);
              match(DIVINT);
              setState(50);
              match(REG);
              setState(51);
              match(REG);
            }
            break;
          case DIVMOD:
            {
              setState(52);
              match(DIVMOD);
              setState(53);
              match(REG);
              setState(54);
              match(REG);
            }
            break;
          case DIVI:
            {
              setState(55);
              match(DIVI);
              setState(56);
              match(REG);
              setState(57);
              match(NUMBER);
            }
            break;
          case PUSH:
            {
              setState(58);
              match(PUSH);
              setState(61);
              _errHandler.sync(this);
              switch (_input.LA(1)) {
                case NUMBER:
                  {
                    setState(59);
                    ((InstructionContext) _localctx).n = match(NUMBER);
                  }
                  break;
                case LABEL:
                  {
                    setState(60);
                    ((InstructionContext) _localctx).l = match(LABEL);
                  }
                  break;
                default:
                  throw new NoViableAltException(this);
              }
            }
            break;
          case PUSHR:
            {
              setState(63);
              match(PUSHR);
              setState(64);
              match(REG);
            }
            break;
          case POP:
            {
              setState(65);
              match(POP);
            }
            break;
          case POPR:
            {
              setState(66);
              match(POPR);
              setState(67);
              match(REG);
            }
            break;
          case BRANCH:
            {
              setState(68);
              match(BRANCH);
              setState(69);
              match(LABEL);
            }
            break;
          case BRANCHEQ:
            {
              setState(70);
              match(BRANCHEQ);
              setState(71);
              match(REG);
              setState(72);
              match(REG);
              setState(73);
              match(LABEL);
            }
            break;
          case BRANCHLESS:
            {
              setState(74);
              match(BRANCHLESS);
              setState(75);
              match(REG);
              setState(76);
              match(REG);
              setState(77);
              match(LABEL);
            }
            break;
          case BRANCHLESSEQ:
            {
              setState(78);
              match(BRANCHLESSEQ);
              setState(79);
              match(REG);
              setState(80);
              match(REG);
              setState(81);
              match(LABEL);
            }
            break;
          case JUMPSUB:
            {
              setState(82);
              match(JUMPSUB);
              setState(83);
              match(LABEL);
            }
            break;
          case RETURNSUB:
            {
              setState(84);
              match(RETURNSUB);
              setState(85);
              match(REG);
            }
            break;
          case LABEL:
            {
              setState(86);
              ((InstructionContext) _localctx).l = match(LABEL);
              setState(87);
              match(T__2);
            }
            break;
          case HALT:
            {
              setState(88);
              match(HALT);
            }
            break;
          default:
            throw new NoViableAltException(this);
        }
      }
    } catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    } finally {
      exitRule();
    }
    return _localctx;
  }

  public static final String _serializedATN =
    "\u0004\u0001\"\\\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0001" +
    "\u0000\u0005\u0000\u0006\b\u0000\n\u0000\f\u0000\t\t\u0000\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0003\u0001>\b\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001" +
    "\u0003\u0001Z\b\u0001\u0001\u0001\u0000\u0000\u0002\u0000\u0002\u0000" +
    "\u0000t\u0000\u0007\u0001\u0000\u0000\u0000\u0002Y\u0001\u0000\u0000\u0000" +
    "\u0004\u0006\u0003\u0002\u0001\u0000\u0005\u0004\u0001\u0000\u0000\u0000" +
    "\u0006\t\u0001\u0000\u0000\u0000\u0007\u0005\u0001\u0000\u0000\u0000\u0007" +
    "\b\u0001\u0000\u0000\u0000\b\u0001\u0001\u0000\u0000\u0000\t\u0007\u0001" +
    "\u0000\u0000\u0000\n\u000b\u0005\u0004\u0000\u0000\u000b\f\u0005\u001d" +
    "\u0000\u0000\f\r\u0005\u001f\u0000\u0000\r\u000e\u0005\u0001\u0000\u0000" +
    "\u000e\u000f\u0005\u001d\u0000\u0000\u000fZ\u0005\u0002\u0000\u0000\u0010" +
    "\u0011\u0005\u0005\u0000\u0000\u0011\u0012\u0005\u001d\u0000\u0000\u0012" +
    "\u0013\u0005\u001f\u0000\u0000\u0013\u0014\u0005\u0001\u0000\u0000\u0014" +
    "\u0015\u0005\u001d\u0000\u0000\u0015Z\u0005\u0002\u0000\u0000\u0016\u0017" +
    "\u0005\u0006\u0000\u0000\u0017\u0018\u0005\u001d\u0000\u0000\u0018Z\u0005" +
    "\u001f\u0000\u0000\u0019\u001a\u0005\u0007\u0000\u0000\u001a\u001b\u0005" +
    "\u001d\u0000\u0000\u001bZ\u0005\u001d\u0000\u0000\u001c\u001d\u0005\b" +
    "\u0000\u0000\u001d\u001e\u0005\u001d\u0000\u0000\u001eZ\u0005\u001d\u0000" +
    "\u0000\u001f \u0005\t\u0000\u0000 !\u0005\u001d\u0000\u0000!Z\u0005\u001f" +
    "\u0000\u0000\"#\u0005\n\u0000\u0000#$\u0005\u001d\u0000\u0000$Z\u0005" +
    "\u001d\u0000\u0000%&\u0005\u000b\u0000\u0000&\'\u0005\u001d\u0000\u0000" +
    "\'Z\u0005\u001f\u0000\u0000()\u0005\f\u0000\u0000)*\u0005\u001d\u0000" +
    "\u0000*Z\u0005\u001d\u0000\u0000+,\u0005\r\u0000\u0000,-\u0005\u001d\u0000" +
    "\u0000-Z\u0005\u001f\u0000\u0000./\u0005\u000e\u0000\u0000/0\u0005\u001d" +
    "\u0000\u00000Z\u0005\u001d\u0000\u000012\u0005\u0010\u0000\u000023\u0005" +
    "\u001d\u0000\u00003Z\u0005\u001d\u0000\u000045\u0005\u0011\u0000\u0000" +
    "56\u0005\u001d\u0000\u00006Z\u0005\u001d\u0000\u000078\u0005\u000f\u0000" +
    "\u000089\u0005\u001d\u0000\u00009Z\u0005\u001f\u0000\u0000:=\u0005\u0012" +
    "\u0000\u0000;>\u0005\u001f\u0000\u0000<>\u0005\u001e\u0000\u0000=;\u0001" +
    "\u0000\u0000\u0000=<\u0001\u0000\u0000\u0000>Z\u0001\u0000\u0000\u0000" +
    "?@\u0005\u0013\u0000\u0000@Z\u0005\u001d\u0000\u0000AZ\u0005\u0014\u0000" +
    "\u0000BC\u0005\u0015\u0000\u0000CZ\u0005\u001d\u0000\u0000DE\u0005\u0016" +
    "\u0000\u0000EZ\u0005\u001e\u0000\u0000FG\u0005\u0017\u0000\u0000GH\u0005" +
    "\u001d\u0000\u0000HI\u0005\u001d\u0000\u0000IZ\u0005\u001e\u0000\u0000" +
    "JK\u0005\u0018\u0000\u0000KL\u0005\u001d\u0000\u0000LM\u0005\u001d\u0000" +
    "\u0000MZ\u0005\u001e\u0000\u0000NO\u0005\u0019\u0000\u0000OP\u0005\u001d" +
    "\u0000\u0000PQ\u0005\u001d\u0000\u0000QZ\u0005\u001e\u0000\u0000RS\u0005" +
    "\u001a\u0000\u0000SZ\u0005\u001e\u0000\u0000TU\u0005\u001b\u0000\u0000" +
    "UZ\u0005\u001d\u0000\u0000VW\u0005\u001e\u0000\u0000WZ\u0005\u0003\u0000" +
    "\u0000XZ\u0005\u001c\u0000\u0000Y\n\u0001\u0000\u0000\u0000Y\u0010\u0001" +
    "\u0000\u0000\u0000Y\u0016\u0001\u0000\u0000\u0000Y\u0019\u0001\u0000\u0000" +
    "\u0000Y\u001c\u0001\u0000\u0000\u0000Y\u001f\u0001\u0000\u0000\u0000Y" +
    "\"\u0001\u0000\u0000\u0000Y%\u0001\u0000\u0000\u0000Y(\u0001\u0000\u0000" +
    "\u0000Y+\u0001\u0000\u0000\u0000Y.\u0001\u0000\u0000\u0000Y1\u0001\u0000" +
    "\u0000\u0000Y4\u0001\u0000\u0000\u0000Y7\u0001\u0000\u0000\u0000Y:\u0001" +
    "\u0000\u0000\u0000Y?\u0001\u0000\u0000\u0000YA\u0001\u0000\u0000\u0000" +
    "YB\u0001\u0000\u0000\u0000YD\u0001\u0000\u0000\u0000YF\u0001\u0000\u0000" +
    "\u0000YJ\u0001\u0000\u0000\u0000YN\u0001\u0000\u0000\u0000YR\u0001\u0000" +
    "\u0000\u0000YT\u0001\u0000\u0000\u0000YV\u0001\u0000\u0000\u0000YX\u0001" +
    "\u0000\u0000\u0000Z\u0003\u0001\u0000\u0000\u0000\u0003\u0007=Y";
  public static final ATN _ATN = new ATNDeserializer()
    .deserialize(_serializedATN.toCharArray());

  static {
    _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
      _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
    }
  }
}
