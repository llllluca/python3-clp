package src.codeGeneration;

import java.util.ArrayList;
import org.antlr.v4.runtime.tree.ParseTree;
import src.parser.Python3Parser.ArgumentContext;
import src.parser.Python3Parser.AssignmentContext;
import src.parser.Python3Parser.AtomContext;
import src.parser.Python3Parser.BlockContext;
import src.parser.Python3Parser.Compound_stmtContext;
import src.parser.Python3Parser.ExprContext;
import src.parser.Python3Parser.FuncdefContext;
import src.parser.Python3Parser.If_stmtContext;
import src.parser.Python3Parser.Return_stmtContext;
import src.parser.Python3Parser.RootContext;
import src.parser.Python3ParserBaseVisitor;
import src.semanticAnalysis.FuncSymbol;
import src.semanticAnalysis.Scope;
import src.semanticAnalysis.SemanticAnalysisVisitor;
import src.semanticAnalysis.VariableSymbol;

public class CodeGenVisitor extends Python3ParserBaseVisitor<String> {

  private int count = 0;
  private SemanticAnalysisVisitor semVis;
  private Scope currentScope;
  private FuncdefContext currentFuncdefContext = null;

  public CodeGenVisitor(SemanticAnalysisVisitor S) {
    semVis = S;
    this.currentScope = S.currentScope;
  }

  @Override
  public String visitBlock(BlockContext ctx) {
    currentScope = semVis.scopes.get(ctx);
    String code = visitChildren(ctx);
    currentScope = currentScope.getEnclosingScope();
    return code;
  }

  public String newLabel() {
    String base = "label";
    return base + count++;
  }

  @Override
  protected String aggregateResult(String aggregate, String nextResult) {
    if (aggregate == null) return nextResult;
    else if (nextResult == null) return aggregate;
    else return aggregate + nextResult;
  }

  @Override
  public String visitRoot(RootContext ctx) {
    String code = "";
    code += "push 999\n";
    code += "push 999\n";
    code += "subi SP " + semVis.offset.get(ctx) + "\n";
    ArrayList<FuncdefContext> defs = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); i++) {
      ParseTree child = ctx.getChild(i);
      if (
        child instanceof Compound_stmtContext &&
        ((Compound_stmtContext) child).funcdef() != null
      ) {
        defs.add(((Compound_stmtContext) child).funcdef());
      } else {
        String childCode = visit(child);
        if (childCode != null) {
          code += childCode;
        }
      }
    }
    code += "halt\n";

    for (FuncdefContext f : defs) {
      this.currentFuncdefContext = f;
      code += visit(f);
    }
    return code;
  }

  @Override
  public String visitExpr(ExprContext ctx) {
    String code = "";
    if (ctx.expr().size() == 2) {
      if (!ctx.ADD().isEmpty()) {
        code = cgenAdd(ctx);
      } else if (!ctx.MINUS().isEmpty()) {
        code = cgenMinus(ctx);
      } else if (ctx.STAR() != null) {
        code = cgenMul(ctx);
      } else if (ctx.DIV() != null) {
        code = cgenDivInt(ctx);
      } else if (ctx.MOD() != null) {
        code = cgenMod(ctx);
      } else if (ctx.AND() != null) {
        code = genAnd(ctx);
      } else if (ctx.OR() != null) {
        code = genOr(ctx);
      } else if (ctx.comp_op() != null) {
        if (ctx.comp_op().GREATER_THAN() != null) {
          code = cgenGreater(ctx);
        } else if (ctx.comp_op().LESS_THAN() != null) {
          code = cgenLess(ctx);
        } else if (ctx.comp_op().GT_EQ() != null) {
          code = cgenGreaterEqual(ctx);
        } else if (ctx.comp_op().LT_EQ() != null) {
          code = cgenLessEqual(ctx);
        } else if (ctx.comp_op().EQUALS() != null) {
          code = cgenEqual(ctx);
        } else if (
          ctx.comp_op().NOT_EQ_1() != null || ctx.comp_op().NOT_EQ_2() != null
        ) {
          code = cgenNotEqual(ctx);
        }
      }
    } else if (ctx.atom() != null) {
      if (ctx.atom().NAME() != null && ctx.trailer(0) != null) {
        // lookup per la funzione
        code = cgenFuncInvk(ctx);
      } else {
        code = visit(ctx.atom());
      }
    } else if (ctx.NOT() != null) {
      code = cgenNot(ctx);
    } else {
      code = visitChildren(ctx);
    }
    return code;
  }

  public String cgenFuncInvk(ExprContext ctx) {
    String name = ctx.atom().NAME().getText();
    FuncSymbol func = (FuncSymbol) currentScope.resolve_recursively(name);
    String funcLabel = func.getLabel();
    String code = "";
    code += "pushr FP\n";
    code += "move SP FP\n";
    code += "addi FP 1\n";
    code += "move AL T1\n";
    // risalire AL
    int actualNesting = currentScope.getNestingLevel();
    int funcNesting = func.getScope().getNestingLevel();
    for (int i = 0; i < (actualNesting - funcNesting); i++) {
      code += "store T1 0(T1)\n";
    }
    code += "pushr T1\n";
    if (ctx.trailer(0).arglist() != null) {
      for (ArgumentContext e : ctx.trailer(0).arglist().argument()) {
        code += visit(e);
        code += "pushr A0\n";
      }
    }
    code += "move FP AL\n";
    code += "subi AL 1\n";
    code += "jsub ";
    code += funcLabel;
    code += "\n";
    return code;
  }

  public String visitArgument(ArgumentContext ctx) {
    if (ctx.expr() != null) {
      return visit(ctx.expr(0));
    } else {
      return "";
    }
  }

  public String cgenAdd(ExprContext ctx) {
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "add T1 A0\n" +
      "popr A0\n"
    );
  }

  public String cgenMinus(ExprContext ctx) {
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "sub T1 A0\n" +
      "popr A0\n"
    );
  }

  public String cgenMul(ExprContext ctx) {
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "mul A0 T1\n" +
      "popr A0\n"
    );
  }

  public String cgenDivInt(ExprContext ctx) {
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "divint T1 A0\n" +
      "popr A0\n"
    );
  }

  public String cgenMod(ExprContext ctx) {
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "divmod T1 A0\n" +
      "popr A0\n"
    );
  }

  public String genAnd(ExprContext ctx) {
    String labelEnd = newLabel();
    return (
      visit(ctx.expr(0)) +
      "storei T1 0" +
      "beq A0 T1 " +
      labelEnd +
      "\n" +
      visit(ctx.expr(1)) +
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String genOr(ExprContext ctx) {
    String labelEnd = newLabel();
    return (
      visit(ctx.expr(0)) +
      "storei T1 1\n" +
      "beq A0 T1 " +
      labelEnd +
      "\n" +
      visit(ctx.expr(1)) +
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenNot(ExprContext ctx) {
    String labelEnd = newLabel();
    String label = newLabel();
    return (
      visit(ctx.expr(0)) +
      "storei T1 0" +
      "beq A0 T1 " +
      label +
      "\n" + //e=0
      "storei A0 0\n" +
      "b " +
      labelEnd +
      "\n" +
      label +
      ":\n" +
      "storei A0 1\n" + // A0=!0
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenGreater(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelFalse = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "bleq T1 A0 " +
      labelFalse +
      "\n" + // if (T1 <= A0) goto labelEnd
      "storei A0 1\n" + // T1 > A0
      "b " +
      labelEnd +
      "\n" +
      labelFalse +
      ": " +
      "storei A0 0\n" + // T1 <= A0
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenLess(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelFalse = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "bleq A0 T1 " +
      labelFalse +
      "\n" + // if (A0 <= T1) goto labelEnd
      "storei A0 1\n" + // A0 < T1
      "b " +
      labelEnd +
      "\n" +
      labelFalse +
      ":\n" +
      "storei A0 0\n" + // A0 >= T1
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenGreaterEqual(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelFalse = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "bl T1 A0 " +
      labelFalse +
      "\n" + // if (T1 < A0) goto labelEnd
      "storei A0 1\n" + // T1 > A0
      "b " +
      labelEnd +
      "\n" +
      labelFalse +
      ":\n" +
      "storei A0 0\n" + // T1 <= A0
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenLessEqual(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelTrue = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "bleq T1 A0 " +
      labelTrue +
      "\n" +
      "storei A0 0\n" +
      "b " +
      labelEnd +
      "\n" +
      labelTrue +
      ":\n" +
      "storei A0 1\n" +
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenNotEqual(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelFalse = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "beq T1 A0 " +
      labelFalse +
      "\n" +
      "storei A0 1\n" +
      "b " +
      labelEnd +
      "\n" +
      labelFalse +
      ":\n" +
      "storei A0 0\n" +
      labelEnd +
      ": " +
      "\n"
    );
  }

  public String cgenEqual(ExprContext ctx) {
    String labelEnd = newLabel();
    String labelTrue = newLabel();
    return (
      visit(ctx.expr(0)) +
      "pushr A0\n" +
      visit(ctx.expr(1)) +
      "popr T1\n" +
      "beq T1 A0 " +
      labelTrue +
      "\n" +
      "storei A0 0\n" +
      "b " +
      labelEnd +
      "\n" +
      labelTrue +
      ":\n" +
      "storei A0 1\n" +
      labelEnd +
      ": " +
      "\n"
    );
  }

  @Override
  public String visitAtom(AtomContext ctx) {
    String code = "";
    if (ctx.NUMBER() != null) {
      code = "storei A0 " + ctx.NUMBER().getText() + "\n";
    } else if (ctx.NAME() != null) {
      String name = ctx.NAME().getText();
      VariableSymbol var = (VariableSymbol) currentScope.resolve_recursively(
        name
      );
      int actualNesting = currentScope.getNestingLevel();
      int funcNesting = var.getScope().getNestingLevel();
      code += "move AL T1\n";
      for (int i = 0; i < (actualNesting - funcNesting); i++) {
        code += "store T1 0(T1)\n";
      }
      code += "subi T1 " + var.getOffset() + "\n";
      code += "store A0 0(T1)\n";
    } else if (ctx.TRUE() != null) {
      code = "storei A0 1\n";
    } else if (ctx.FALSE() != null) {
      code = "storei A0 0\n";
    } else {
      code = visitChildren(ctx);
    }
    return code;
  }

  @Override
  public String visitAssignment(AssignmentContext ctx) {
    String code = "";
    code += visit(ctx.exprlist(1));
    // only '=' is possible
    assert (ctx.augassign().ASSIGN().getText().equals("="));
    String name = ctx.exprlist(0).expr(0).atom().NAME().getText();
    VariableSymbol var = (VariableSymbol) currentScope.resolve_recursively(
      name
    );
    int actualNesting = currentScope.getNestingLevel();
    int funcNesting = var.getScope().getNestingLevel();
    code += "move AL T1\n";
    for (int i = 0; i < (actualNesting - funcNesting); i++) {
      code += "store T1 0(T1)\n";
    }
    code += "subi T1 " + var.getOffset() + "\n";
    code += "load A0 0(T1)\n";
    return code;
  }

  public String cgenIfElse(If_stmtContext ctx) {
    String trueBranch = newLabel();
    String endIf = newLabel();

    if (ctx.ELSE() != null) {
      return (
        visit(ctx.expr(0)) +
        "storei T1 1\n" +
        "beq A0 T1 " +
        trueBranch +
        "\n" +
        visit(ctx.block(1)) +
        "b " +
        endIf +
        "\n" +
        trueBranch +
        ": " +
        visit(ctx.block(0)) +
        endIf +
        ": \n"
      );
    } else {
      return (
        visit(ctx.expr(0)) +
        "storei T1 0\n" +
        "beq A0 T1 " +
        endIf +
        "\n" +
        visit(ctx.block(0)) +
        endIf +
        ": \n"
      );
    }
  }

  @Override
  public String visitIf_stmt(If_stmtContext ctx) {
    assert (ctx.expr().size() == 1);
    return cgenIfElse(ctx);
  }

  @Override
  public String visitFuncdef(FuncdefContext ctx) {
    currentScope = semVis.scopes.get(ctx);
    FuncSymbol func = (FuncSymbol) currentScope.resolve_recursively(
      ctx.NAME().getText()
    );
    int localVar = this.semVis.offset.get(ctx) - func.getParamLength();
    int totalOffset = func.getParamLength() + localVar;
    String funcLabel = func.getLabel();
    String code = "";
    code += funcLabel;
    code += " :\n";
    code += "subi SP " + localVar + "\n";
    code += "pushr RA\n";
    code += visit(ctx.block());
    code += "end" + funcLabel + ":\n";
    code += "popr RA\n";
    code += "addi SP " + totalOffset + "\n";
    code += "pop\n";
    code += "store FP 0(FP)\n";
    code += "move FP AL\n";
    code += "subi AL 1\n";
    code += "pop\n";
    code += "rsub RA\n";

    currentScope = currentScope.getEnclosingScope();
    return code;
  }

  @Override
  public String visitReturn_stmt(Return_stmtContext ctx) {
    String name = currentFuncdefContext.NAME().getText();
    FuncSymbol func = (FuncSymbol) currentScope.resolve_recursively(name);
    String funcLabel = func.getLabel();
    return visit(ctx.exprlist()) + "b end" + funcLabel + "\n";
  }
}
