#!/bin/bash

for i in $(ls progs/ok_sem/ | grep .py); do
    echo $i
    make runsem ARGS=progs/ok_sem/$i &> error.txt
    output=$(<error.txt)
    if [[ "$output" =~ "Error" ]]; then
        echo "Error!"
        exit 1
    fi
done
