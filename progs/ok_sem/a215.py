def clear_tuple(test_tup):
  temp = list(test_tup)
  temp.clear()
  test_tup = tuple(temp)
  return (test_tup) 