def alternate_elements(list1):
    result=[]
    for item in list1[::2]:
        result.append(item)
    return result 