def foo(x):
    d=[]
    temp=x
    n=0
    k = 1
    while (temp>0):
        m = k+100
        d.append(temp%10)
        temp=int(temp/10)
        n+=1
    d.reverse()
    b=0
    i=n
    while (b<x):
        b=0
        for j in range(1,n+1):
            b+=d[i-j]
        d.append(b)
        i=i+1
    return(b==x)