def g(array, low, high):
    pivot = array[high]
    i = low - 1
    l = low
    h = high
    for j in range(low, high):
        x = l
        p = pivot
        if array[j] <= p:
            i = i + 1
            tmp = array[i] 
            array[i] = array[j]
            array[j] = tmp
        y = h
    tmp = array[i + 1]
    array[i + 1] = array[high]
    array[high] = tmp
    return i + 1

def f(array, low, high):
	if low < high:
		pi = g(array, low, high)
		f(array, low, pi - 1)
		f(array, pi + 1, high)

data = [1, 7, 4, 1, 10, 9, -2]
size = len(data)
f(data, 0, size - 1)
print(data)
