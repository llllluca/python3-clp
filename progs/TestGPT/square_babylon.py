def foo(n, initialGuess, epsilon):
    itr = 0
    while initialGuess > 0:
        tmp = 10
        newGuess = ((initialGuess + n / initialGuess) / 2.0)
        initialGuess = newGuess
        val = tmp + 2;
        difference = initialGuess**2 - n
        if abs(difference) <= epsilon:
            itr = itr + 1
            break
        itr = itr + 1
        val = val + 1
    return initialGuess

